const { getFileInformation } = require('./s3');
const { resize } = require('./resize');
const { transform } = require('./transform');

const DEFAULT_SIZE_LIST = [
  '123x123',
  '155x155',
  '158x161',
  '246x246',
  '310x310',
  '316x322',
  '348x348',
  '492x492',
  '632x644',
  '696x696',
  '789x789',
];

module.exports.resizer = async (event, context, callback) => {
  const { eventName, bucket, key } = getFileInformation(event);

  if (DEFAULT_SIZE_LIST.find((el) => key.includes(el))) return;

  console.log(
    `Received ${eventName} for item in bucket: ${bucket}, key: ${key}`
  );

  try {
    await resize(bucket, key, DEFAULT_SIZE_LIST);
  } catch (error) {
    callback(error);
  }
};

module.exports.transformer = async (event, context, callback) => {
  const { eventName, bucket, key } = getFileInformation(event);

  console.log(
    `Received ${eventName} for item in bucket: ${bucket}, key: ${key}`
  );
  try {
    await transform(bucket, key);
  } catch (error) {
    callback(error);
  }
};
