module.exports.filename = (path) => {
  return path.split('.').slice(0, -1).join('.');
};

const extension = (path) => {
  // eslint-disable-next-line no-bitwise
  return path.slice(((path.lastIndexOf('.') - 1) >>> 0) + 2);
};

module.exports.filenameWithSize = (path, size) => {
  return `${this.filename(path)}_${size}.${extension(path)}`;
};
