const sharp = require('sharp');

const { getObjectStream, uploadStream } = require('./s3');
const { filenameWithSize } = require('./helpers');

const getDimension = (dimension) => {
  const dimensionArray = dimension.split('x');

  return {
    width: parseInt(dimensionArray[0], 10),
    height: parseInt(dimensionArray[1], 10),
  };
};

module.exports.resize = (bucket, key, sizeList) => {
  return new Promise((resolve, reject) => {
    const readStream = getObjectStream(bucket, key);
    Promise.all(
      sizeList.map(async (size) => {
        const { width, height } = getDimension(size);

        const targetKey = filenameWithSize(key, size);

        const resizeStream = sharp().resize(width, height);

        const { writeStream, uploaded } = uploadStream(bucket, targetKey);

        readStream.pipe(resizeStream).pipe(writeStream);

        await uploaded;
      })
    )
      .then(resolve)
      .catch(reject);
  });
};
