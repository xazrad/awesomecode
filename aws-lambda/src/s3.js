const stream = require('stream');
const AWS = require('aws-sdk');
const mime = require('mime/lite');

const s3 = new AWS.S3();

module.exports.getObjectStream = (Bucket, Key) => {
  console.log(`Streaming ${Key} data from ${Bucket}`);

  return s3.getObject({ Bucket, Key }).createReadStream();
};

module.exports.deleteObject = (Bucket, Key) => {
  return s3.deleteObject({ Bucket, Key }).promise();
};

module.exports.uploadStream = (Bucket, Key) => {
  console.log(`Uploading ${Key} to ${Bucket} via stream data`);

  const passThrough = new stream.PassThrough();

  return {
    writeStream: passThrough,
    uploaded: s3
      .upload({
        Bucket,
        Key,
        Body: passThrough,
        ContentType: mime.getType(Key),
      })
      .promise(),
  };
};

module.exports.getFileInformation = ({
  Records: [
    {
      eventName,
      s3: { bucket, object },
    },
  ],
}) => {
  return {
    eventName,
    bucket: bucket.name,
    key: decodeURIComponent(object.key).replace(/\+/g, ' '),
  };
};
