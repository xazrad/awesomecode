const sharp = require('sharp');
const { deleteObject, getObjectStream, uploadStream } = require('./s3');
const { filename } = require('./helpers');

const upload = (bucket, key) => {
  const readStream = getObjectStream(bucket, key);
  const transformStream = sharp().jpeg({ quality: 70 });

  let targetKey = key.replace('images-original/', 'images/');
  targetKey = `${filename(targetKey)}.jpg`;

  const { writeStream, uploaded } = uploadStream(bucket, targetKey);
  readStream.pipe(transformStream).pipe(writeStream);
  return uploaded;
};

module.exports.transform = (bucket, key) =>
  upload(bucket, key).then(() => deleteObject(bucket, key));
