import React, { Component, Fragment} from 'react'
import { connect } from 'react-redux';
import { ReccomendCard, ButtonRedisigne } from 'universal/components/UI';

class Favorites extends Component {
    handleLoadMore = () => {
        api.get(`/api/v1/favorites?offset=${this.props.favorites.offset}`).then((res) => {
            this.props.loadMore(res.data.favorites)
            this.props.changeMore(res.data.more)
        }).catch(error => {
            console.log(error);
        });
    }

    deliteFavorite = (article) => {
        api.post(`/api/v1/favorites/delete?article=${article}`).then((data) => {
            let newArt = this.props.favorites.articles.filter(item => {
                return item.article !== article
            })

            this.props.changeFavorites(newArt);
            this.props.upCountFavorites(data.data.articlesQuantity);
        }).catch(error => {
            console.log(error);
        });
    }

    render () {
        const { count, favorites} = this.props;

        return (
            <Fragment>
                <h1>Favorites</h1>
                { favorites.articles && !favorites.articles.length &&
                    <span>Please Add Favorites.</span>
                }
                <div className="favorites__cards">
                    { favorites.articles && favorites.articles.map((card, id) => {
                        return <ReccomendCard { ...card } deliteFavorite={ this.deliteFavorite } type="favorites" img={ card.image } descr={ true } key={ card.article } />
                    })}
                </div>
                { favorites.more === true && count >= favorites.offset &&
                    <ButtonRedisigne className="favorites__more" type="reset" onClick={ this.handleLoadMore }>Add More</ButtonRedisigne>
                }
            </Fragment>
        )
    }
}

export default connect(
    state => ({
        favorites: state.favorites.articles,
        count: state.favorites.count
    }),
    dispatch => ({
        changeFavorites: (article) => {
            dispatch({type: 'CHANGE_FAVORITES', payload: article})
        },
        loadMore: (article) => {
            dispatch({type: 'LOAD_MORE', payload: article})
        },
        changeMore: (item) => {
            dispatch({type: 'CHANGE_MORE', payload: item})
        },
        upCountFavorites: (data) => {
            dispatch({type: 'UP_COUNT_FAVORITES', payload: data})
        }
    })
)(Favorites)
