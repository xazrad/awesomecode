import React, { Component, Fragment} from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Helmet } from 'react-helmet';
import Page from 'universal/components/widgets/Page';
import { Link } from 'universal/components/UI';
import FavoritesMenu from './components/MobileMenu';
import AuthPopUp from './AuthPopUp';
import Favorites from './favorites';
import Start from './start';
import api from 'universal/libs/api';

import './style.scss';

class Profile extends Component {
    state = {
        isAuthFormOpen: false
    }

    componentDidUpdate (prevProps) {
        const { props } = this;

        if (props && props.user.authorized && prevProps.user && !prevProps.user.authorized) {
            location.reload();
        }
    }

    handleLogout = () => {
        api.post(`/api/v1/profile/logout`).then((data) => {
            location.reload();
        }).catch(error => {
            console.log(error);
        });
    }

    handleTooglePopUp = () => {
        this.setState((state) => ({
            isAuthFormOpen: !state.isAuthFormOpen
        }));
    }

    render () {
        const { user, match } = this.props;
        const { isAuthFormOpen} = this.state;

        return (
            <Page>
                <Helmet title="Control Panel" />
                <div className="favorites">
                    <FavoritesMenu user={ user } />
                    <aside className="favorites__aside">
                        { user.authorized ?
                            <Fragment>
                                <h3>{`${user.lastName} ${user.firstName} ${user.thirdName}`}</h3>
                                <span className="favorites__under">{ user.email }</span>
                                <span className="favorites__under">{ user.phone }</span>
                                <div className="bonus">
                                    <span>Бонусы: </span>
                                    <span className="favorites__under"> {user.bonusBalance} руб.</span>
                                </div>
                            </Fragment> :
                                <Link className="reg" onClick={ this.handleTooglePopUp }>SignIn</Link>
                        }
                        <div className="br" />
                        <a href="/profile/favorites" className="favorites__a">Faforites</a>
                        <br />
                        { user.authorized &&
                            <span onClick={ this.handleLogout } className="favorites__a">Logout</span>
                        }
                    </aside>
                    <main className="favorites__main">
                        { isAuthFormOpen &&
                            <AuthPopUp onClose={ this.handleTooglePopUp } />
                        }
                        { user.authorized &&
                            <Switch>
                                <Route exact path={ `${match.url}` } component={ Start } />
                                <Route exact path={ `${match.url}/favorites` } component={ Favorites } />
                            </Switch>
                        }
                    </main>
                </div>
            </Page>
        )
    }
}

export default connect(
    state => ({
        user: state.user,
    })
)(Profile);
